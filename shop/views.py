from django.shortcuts import render
from django.views import View

class Shop(View):
    def get(self, request):
        return render(request, 'shop/shop.html')
